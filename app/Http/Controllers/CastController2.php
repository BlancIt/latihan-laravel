<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

// This is the old CastController class, which uses Query Builder 

class CastController extends Controller
{

    public function index() {
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function create() {
        return view('cast.createcast');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');

    }

    public function show($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail', compact('cast'));
    }

    public function edit($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
        ->where('id',$id)
        ->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function destroy($id) {
        $cast = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
