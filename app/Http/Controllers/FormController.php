<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form() {
        return view('halaman.form');
    }

    public function submit(Request $request) {
        $name = $request['fname'] . " " . $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $spokenlanguage = $request['spokenl'];
        $bio = $request['bio'];

        return view('halaman.welcome', compact('name','gender','nationality','spokenlanguage','bio'));
    }
}
