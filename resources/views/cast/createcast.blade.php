@extends('layout.master')

@section('title')
Create Cast
@endsection

@section('content')

<h2>Buat Account!</h2><br>
<form action="/cast" method="post">
    @csrf
	<div class="form-group">
		<label for="nama">Nama:</label><br>
		<input type="text" id="nama" name="nama" class="form-control" required /><br>
	</div>

	<div class="form-group">
		<label for="umur">Umur:</label><br>
		<input type="number" id="umur" name="umur" class="form-control" required /><br>
	</div>

	<div class="form-group">
		<label for="bio">Bio</label><br>
		<textarea id="bio" name="bio" rows="10" cols="50" class="form-control" required></textarea><br>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
