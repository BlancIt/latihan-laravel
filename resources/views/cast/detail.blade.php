@extends('layout.master')

@section('title')
Detail dari Cast #{{$cast->id}}
@endsection

@section('content')

<div class="card">
    <div class="card-body">
      <h5 class="card-title">{{$cast->nama}} (Age {{$cast->umur}})</h5><br>
      <p class="card-text">{{$cast->bio}}</p>
    </div>
   
</div>
<div class="row justify-content-center">
    <a href='/cast' class="btn btn-info mb-3 text-center">Kembali</a>
</div>
@endsection
