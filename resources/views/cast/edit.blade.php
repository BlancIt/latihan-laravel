@extends('layout.master')

@section('title')
Edit Cast #{{$cast->id}}
@endsection

@section('content')

<h2>Buat Account!</h2><br>
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('PUT')
	<div class="form-group">
		<label for="nama">Nama:</label><br>
		<input type="text" id="nama" name="nama" value="{{$cast->nama}}" class="form-control" required /><br>
	</div>

	<div class="form-group">
		<label for="umur">Umur:</label><br>
		<input type="number" id="umur" name="umur" value="{{$cast->umur}}" class="form-control" required /><br>
	</div>

	<div class="form-group">
		<label for="bio">Bio</label><br>
		<textarea id="bio" name="bio" rows="10" cols="50" class="form-control" required>{{$cast->bio}}</textarea><br>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
