@extends('layout.master')

@section('title')
Media Online
@endsection

@section('content')

<h2>Buat Account!</h2><br>
<form action="/welcome" method="post">
    @csrf
	
	<label for="fname">First name:</label><br>
	<input type="text" id="fname" name="fname" placeholder="Enter first name" required /><br><br>
	
	<label for="lname">Last name:</label><br>
	<input type="text" id="lname" name="lname" placeholder="Enter last name" required /><br><br>
	
	<label for="gender">Gender:</label><br>
	
	<input type="radio" id="gender1" name="gender" value="Male" checked />
	<label for="gender1">Male</label><br>
	
	<input type="radio" id="gender2" name="gender" value="Female"/>
	<label for="gender2">Female</label><br>
	
	<input type="radio" id="gender3" name="gender" value="Other"/>
	<label for="gender3">Other</label><br><br>
	
	<label for="nationality">Nationality</label><br>
	<select id="nationality" name="nationality">
		<option value="indonesia">Indonesia</option>
		<option value="amerika">Amerika</option>
		<option value="Inggris">Inggris</option>
	</select><br><br>
	
	<label for="spokenl">Language Spoken:</label><br>
	<input type="checkbox" id="language1" name="spokenl" value="Bahasa Indonesia"/>
	<label for="language1">Bahasa Indonesia</label><br>
	<input type="checkbox" id="language2" name="spokenl" value="English"/>
	<label for="language2">English</label><br>
	<input type="checkbox" id="language3" name="spokenl" value="Other"/>
	<label for="language3">Other</label><br><br>

	<label for="bio">Bio</label><br>
	<textarea id="bio" name="bio" rows="10" cols="50"></textarea><br><br>

	<input type="submit" value="Sign Up"/>
</form>
@endsection
